#include <bits/stdc++.h>
using namespace std;

int main() {
  int a, b, k;
  cin >> a >> b >> k;

  int j = k;
  for (int i = min(a, b); i >= 1; i--) {
    if (a % i == 0 && b % i == 0) {
      if (j == 1) {
        cout << i << endl;
        return 0;
      }
      j--;
    }
  }
  return 0;
}
