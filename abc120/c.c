#include <stdio.h>
#include <string.h>

#define MAX_N 100000
#define REMOVED '2'

int main() {
  char S[MAX_N];

  char stack[MAX_N];
  int stack_top;
  int N;
  int has_changed;
  int i;
  int result;

  scanf("%s", S);
  N = strlen(S);
  stack[0] = S[0];
  stack_top = 0;
  result = 0;
  for (i = 1; i < N; i++) {
    if (stack_top >= 0 && S[i] != stack[stack_top]) {
      result += 2;
      stack_top--;
    } else {
      stack_top++;
      stack[stack_top] = S[i];
    }
  }

  printf("%d\n", result);

  return 0;
}
