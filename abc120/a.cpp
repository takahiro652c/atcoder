#include <bits/stdc++.h>
using namespace std;

int main() {
  int a, b, c;
  cin >> a >> b >> c;
  int ans = min(c, b / a);
  cout << ans;
  return 0;
}
