#include <stdio.h>

int main() {
  int A, B, K;

  int i;

  scanf("%d%d%d", &A, &B, &K);
  i = A + 1;
  while (K > 0) {
    i--;
    if (A % i == 0 && B % i == 0) {
      K--;
    }
  }
  printf("%d\n", i);
  return 0;
}
