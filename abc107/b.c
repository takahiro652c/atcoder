#include <stdio.h>

int main() {
  int H, W;

  // 0なら削除対象
  int row_flags[100];
  int col_flags[100];

  int squares[100][100];
  int i, j;
  char str[100];

  scanf("%d %d", &H, &W);

  for (i = 0; i < H; i++) {
    scanf("%s", str);
    for (j = 0; j < W; j++) {
      squares[i][j] = str[j];
      if (str[j] == '#') {
        row_flags[i] = 1;
        col_flags[j] = 1;
      }
    }
  }
  for (i = 0; i < H; i++) {
    if (row_flags[i] == 1) {
      for (j = 0; j < W; j++) {
        if (col_flags[j] == 1) {
          printf("%c", squares[i][j]);
        }
      }
      printf("\n");
    }
  }
  return 0;
}
