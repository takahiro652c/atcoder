// WA
#include <stdio.h>

int main() {
  long int N, K;
  // 負数，正数
  long int n_x[100000], p_x[100000];
  long int n_i = 0, p_i = 0;

  long int i, j, n_tmp, p_tmp;
  long int n_r = 0, p_r = 0;

  scanf("%ld %ld", &N, &K);

  for (i = 0; i < N; i++) {
    scanf("%ld", &n_tmp);
    if (n_tmp < 0) {
      n_x[n_i++] = -n_tmp;
    } else {
      p_x[p_i++] = n_tmp;
      i++;
      break;
    }
  }
  for (; i < N; i++) {
    scanf("%ld", &p_x[p_i++]);
  }

  j = 0;
  n_tmp = n_i == 0 ? -1 : n_x[--n_i];
  p_tmp = p_i == 0 ? -1 : p_x[j];
  for (i = 0; i < K; i++) {
    if (n_tmp < 0 && p_tmp < 0) {
      break;
    } else if (n_tmp < 0) {
      p_r = p_tmp;
      p_tmp = j == p_i - 1 ? -1 : p_x[++j];
    } else if (p_tmp < 0) {
      n_r = n_tmp;
      n_tmp = n_i == 0 ? -1 : n_x[--n_i];
    } else {
      if (n_tmp < p_tmp) {
        n_r = n_tmp;
        n_tmp = n_i == 0 ? -1 : n_x[--n_i];
      } else {
        p_r = p_tmp;
        p_tmp = j == p_i - 1 ? -1 : p_x[++j];
      }
    }
  }

  printf("%ld\n", n_r > p_r ? p_r * 2 + n_r : n_r * 2 + p_r);
  return 0;
}
