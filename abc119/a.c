#include <stdio.h>

int main() {
  int yyyy, mm, dd;
  char S[10];
  scanf("%d/%d/%d", &yyyy, &mm, &dd);
  if (yyyy < 2019) {
    printf("Heisei\n");
    return 0;
  }
  if (yyyy > 2019) {
    printf("TBD\n");
    return 0;
  }
  if (mm < 5) {
    printf("Heisei\n");
    return 0;
  }
  printf("TBD\n");
  return 0;
}
