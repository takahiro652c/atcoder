#include <stdio.h>

int main() {
  int N;
  double x;
  char u[3];

  double sum;
  int i;

  scanf("%d", &N);
  for (i = 0; i < N; i++) {
    scanf("%lf %s", &x, u);
    if (u[0] == 'J') {
      sum += x;
    } else {
      sum += x * 380000.0;
    }
  }
  printf("%f\n", sum);
  return 0;
}
