#include <stdio.h>

int main() {
  int N;
  int x;
  scanf("%d", &N);

  if (N <= 111) {
    x = 111;
  } else if (N <= 222) {
    x = 222;
  } else if (N <= 333) {
    x = 333;
  } else if (N <= 444) {
    x = 444;
  } else if (N <= 555) {
    x = 555;
  } else if (N <= 666) {
    x = 666;
  } else if (N <= 777) {
    x = 777;
  } else if (N <= 888) {
    x = 888;
  } else {
    x = 999;
  }

  printf("%d\n", x);
  return 0;
}
