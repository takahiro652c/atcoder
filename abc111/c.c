#include <stdio.h>
#include <string.h>
#define MAX 100000

int main() {
  long int n;

  long int i, tmp, x, max_odd, max_odd_i, sec_odd, max_even, max_even_i,
      sec_even, y, z;
  long int odd[MAX] = {0}, even[MAX] = {0};

  scanf("%ld", &n);

  for (i = 0; i < n; i++) {
    scanf("%ld", &tmp);
    if (i % 2 == 0) {
      even[tmp - 1]++;
    } else {
      odd[tmp - 1]++;
    }
  }

  max_odd = 0;
  max_odd_i = 0;
  sec_odd = 0;
  for (i = 0; i < MAX; i++) {
    if (odd[i] >= max_odd) {
      sec_odd = max_odd;
      max_odd = odd[i];
      max_odd_i = i;
    } else if (odd[i] > sec_odd) {
      sec_odd = odd[i];
    }
  }
  max_even = 0;
  max_even_i = 0;
  sec_even = 0;
  for (i = 0; i < MAX; i++) {
    if (even[i] >= max_even) {
      sec_even = max_even;
      max_even = even[i];
      max_even_i = i;
    } else if (even[i] > sec_even) {
      sec_even = even[i];
    }
  }

  if (max_odd_i != max_even_i) {
    x = n - max_odd - max_even;
  } else {
    // max_oddにする
    y = n - max_odd - sec_even;
    // max_evenにする
    z = n - sec_odd - max_even;
    x = y < z ? y : z;
  }
  printf("%ld\n", x);
  return 0;
}
