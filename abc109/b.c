#include <stdio.h>
#include <string.h>

int main() {
  int N;
  char Ws[100][11];

  int i, j;

  scanf("%d", &N);
  scanf("%s", Ws[0]);

  for (i = 1; i < N; i++) {
    scanf("%s", Ws[i]);
    if (Ws[i - 1][strlen(Ws[i - 1]) - 1] != Ws[i][0]) {
      printf("No\n");
      return 0;
    } else {
      for (j = 0; j < i; j++) {
        if (strcmp(Ws[i], Ws[j]) == 0) {
          printf("No\n");
          return 0;
        }
      }
    }
  }

  printf("Yes\n");
  return 0;
}
