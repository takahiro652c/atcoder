#include <stdio.h>
#include <stdlib.h>

int gcd(long int a, long int b) {
  if (b == 0) {
    return a;
  }
  return gcd(b, a % b);
}

int compare(const void *a, const void *b) {
  return *(long int *)a - *(long int *)b;
}

int main() {
  long int N, X, x[100000];

  long int i, g;

  scanf("%ld %ld", &N, &X);

  for (i = 0; i < N; i++) {
    scanf("%ld", &x[i]);
    x[i] = x[i] - X;
    if (x[i] < 0) {
      x[i] *= -1;
    }
  }
  if (N == 1) {
    printf("%ld\n", x[0]);
    return 0;
  }

  qsort(x, N, sizeof(long int), compare);

  g = gcd(x[1], x[0]);
  for (i = 2; i < N; i++) {
    g = gcd(x[i], g);
  }

  printf("%ld\n", g);

  return 0;
}
