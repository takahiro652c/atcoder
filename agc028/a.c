#include <stdio.h>
#include <stdlib.h>
#define MAX 100000

int gcd(long int a, long int b) {
  if (b == 0) {
    return a;
  }
  return gcd(b, a % b);
}

int main() {
  long int N, M;

  char S[MAX], T[MAX];

  long int i;

  long int g, m, n, L;

  scanf("%ld %ld", &N, &M);
  scanf("%s", S);
  scanf("%s", T);

  g = gcd(N, M);
  m = M / g;
  n = N / g;
  L = M * N / g;

  for (i = 0; i < g; i++) {
    if (S[i * n] != T[i * m]) {
      printf("-1\n");
      return 0;
    }
  }

  printf("%ld\n", L);

  return 0;
}
