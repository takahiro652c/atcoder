// 未提出
#include <stdio.h>
#include <stdlib.h>
#define MOD 1000000007
#define MAX 100000

long long int A[MAX];
int is_chosen[MAX] = {0};
long int N;

long long int factorial(long int n) {
  long long int result = 1;
  long int k;

  for (k = 1; k <= n; k++) {
    result *= k;
  }
  return result;
}

long long int left_cost(long int choose_index) {
  long long int cost = 0;
  long int i;
  for (i = choose_index - 1; i >= 0; i--) {
    if (is_chosen[i] == 1) {
      return cost;
    } else {
      cost += A[i];
      cost %= MOD;
    }
  }
  return cost;
}

long long int right_cost(long int choose_index) {
  long long int cost = 0;
  long int i;
  for (i = choose_index + 1; i < N; i++) {
    if (is_chosen[i] == 1) {
      return cost;
    } else {
      cost += A[i];
      cost %= MOD;
    }
  }
  return cost;
}

long int rest_left_number(long int choose_index) {
  long int i;
  long int num = 0;
  for (i = 0; i < choose_index; i++) {
    if (is_chosen[i] == 1) {
      continue;
    }
    num++;
  }
  return num;
}
long int rest_right_number(long int choose_index) {
  long int i;
  long int num = 0;
  for (i = choose_index + 1; i < N; i++) {
    if (is_chosen[i] == 1) {
      continue;
    }
    num++;
  }
  return num;
}

long long int calc_cost(long int begin_index, long int end_index) {
  long long int sum = 0;
  long int i = begin_index;
  if (i == end_index) {
    return is_chosen[i] == 1 ? 0 : A[i] + left_cost(i) + right_cost(i);
  }
  for (; i <= end_index; i++) {
    if (is_chosen[i] == 1) {
      continue;
    }
    is_chosen[i] = 1;
    sum += A[i] + left_cost(i) + right_cost(i);
    printf("<%ld %ld %ld>this %lld + %lld + %lld = %lld\n", begin_index,
           end_index, i, A[i], left_cost(i), right_cost(i),
           A[i] + left_cost(i) + right_cost(i));
    sum %= MOD;
    printf("<%ld %ld %ld>left %lld\n", begin_index, end_index, i,
           calc_cost(begin_index, i - 1) * factorial(rest_right_number(i)));
    sum += calc_cost(begin_index, i - 1) * factorial(rest_right_number(i));
    sum %= MOD;
    printf("<%ld %ld %ld>right %lld\n", begin_index, end_index, i,
           calc_cost(i + 1, end_index) * factorial(rest_left_number(i)));
    sum += calc_cost(i + 1, end_index) * factorial(rest_left_number(i));
    sum %= MOD;
    is_chosen[i] = 0;
  }
  printf("sum %lld\n", sum);
  return sum;
}

int main() {
  long int i;

  scanf("%ld", &N);

  for (i = 0; i < N; i++) {
    scanf("%lld", &A[i]);
  }

  printf("\n");
  printf("%lld\n", calc_cost(0, N - 1));

  return 0;
}
