#include <bits/stdc++.h>
using namespace std;

int main() {
  int n;
  cin >> n;

  vector<int> a(n);
  vector<int> b(n);

  for (int i = 0; i < n; i++) {
    cin >> b[i];
  }

  for (int i = 0; i < n; i++) {
    int j;
    for (j = n - i - 1; j >= 0; j--) {
      if (b[j] == j + 1) {
        a[i] = j + 1;
        for (int k = j; k < n - i - 1; k++) {
          b[k] = b[k + 1];
        }
        break;
      }
    }
    if (j < 0) {
      cout << "-1" << endl;
      return 0;
    }
  }

  reverse(a.begin(), a.end());
  for (int x : a) {
    cout << x << endl;
  }

  return 0;
}
