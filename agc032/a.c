#include <stdio.h>
#include <stdlib.h>
#define MAX 100

int main() {
  int N;
  int a[MAX] = {};
  int b[MAX] = {};

  int i, j, k;

  scanf("%d", &N);
  for (i = 0; i < N; i++) {
    scanf("%d", &b[i]);
  }

  for (i = 0; i < N; i++) {
    for (j = N - i - 1; j >= 0; j--) {
      if (b[j] == j + 1) {
        a[i] = j + 1;
        for (k = j; k < N - i - 1; k++) {
          b[k] = b[k + 1];
        }
        break;
      }
    }
    if (j < 0) {
      printf("-1\n");
      return 0;
    }
  }

  for (i = N - 1; i >= 0; i--) {
    printf("%d\n", a[i]);
  }

  return 0;
}
