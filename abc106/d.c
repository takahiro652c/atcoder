#include <stdio.h>

int main() {
  int N, M, Q;
  int l, r;
  int p, q;

  int i, j;
  int counts[500][500];

  scanf("%d %d %d", &N, &M, &Q);

  for (i = 0; i < M; i++) {
    scanf("%d %d", &l, &r);
    counts[l - 1][r - 1]++;
  }

  for (i = N - 1; i > 0; i--) {
    for (j = i; j < N; j++) {
      counts[i - 1][j] += counts[i][j];
    }
  }
  for (i = 0; i < N; i++) {
    for (j = i; j < N - 1; j++) {
      counts[i][j + 1] += counts[i][j];
    }
  }
  for (i = 0; i < Q; i++) {
    scanf("%d %d", &p, &q);
    printf("%d\n", counts[p - 1][q - 1]);
  }

  return 0;
}
