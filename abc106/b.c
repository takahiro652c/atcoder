#include <stdio.h>

int main() {
  int n;
  int i;
  int j;
  int divisor_count;
  int result = 0;

  scanf("%d", &n);

  for (i = 1; i <= n; i += 2) {
    divisor_count = 0;
    for (j = 1; j <= i && divisor_count < 9; j++) {
      if (i % j == 0) {
        divisor_count++;
      }
    }
    if (divisor_count == 8) {
      result++;
    }
  }

  printf("%d\n", result);
  return 0;
}
