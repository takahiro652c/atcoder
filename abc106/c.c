#include <stdio.h>

int main() {
  char s[101];
  long long int k;
  int i;
  int tmp;

  scanf("%s", s);
  scanf("%lld", &k);

  for (i = 0; i < 100; i++) {
    tmp = s[i] - '0';
    if (tmp > 1) {
      printf("%d\n", tmp);
      break;
    } else if (i + 1 == k) {
      printf("%d\n", tmp);
      break;
    }
  }

  return 0;
}
