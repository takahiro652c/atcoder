#include <stdio.h>

int main() {
  long int n;
  unsigned long int s;

  int i, tmp, sum;

  scanf("%ld", &n);

  if (n > 0) {
    s = n;
    while (1) {
      sum = 0;
      tmp = 1;
      for (i = 0; i < 32; i += 2) {
        if (s & (1 << i)) {
          sum += tmp;
        }
        tmp *= 4;
      }
      if (sum < n) {
        // 後は負の数の加算なのでこのsは駄目
        s++;
        continue;
      }

      tmp = 2;
      for (i = 1; i < 32; i += 2) {
        if (s & (1 << i)) {
          sum -= tmp;
        }
        tmp *= 4;
      }
      if (sum == n) {
        break;
      }
      s++;
    }
  } else {
    s = -n;
    while (1) {
      sum = 0;
      tmp = 2;
      for (i = 1; i < 32; i += 2) {
        if (s & (1 << i)) {
          sum -= tmp;
        }
        tmp *= 4;
      }
      if (sum > n) {
        // 後は正の数の加算なのでこのsは駄目
        s++;
        continue;
      }

      tmp = 1;
      for (i = 0; i < 32; i += 2) {
        if (s & (1 << i)) {
          sum += tmp;
        }
        tmp *= 4;
      }
      if (sum == n) {
        break;
      }
      s++;
    }
  }

  i = 31;
  while (!(s & (1 << i)) && i > 0) {
    i--;
  }

  while (i >= 0) {
    printf("%d", s & (1 << i) ? 1 : 0);
    i--;
  }
  printf("\n");
  return 0;
}
