#include <stdio.h>

int main() {
  int n;
  scanf("%d", &n);

  while (n > 0) {
    if (n % 7 == 0) {
      break;
    }
    n -= 4;
  }
  if (n >= 0) {
    printf("Yes\n");
  } else {
    printf("No\n");
  }

  return 0;
}
