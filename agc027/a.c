#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b) {
  return *(long int *)a - *(long int *)b;
}

int main() {
  long int N, x, a[100];

  long int i;

  scanf("%ld %ld", &N, &x);

  for (i = 0; i < N; i++) {
    scanf("%ld", &a[i]);
  }

  qsort(a, N, sizeof(long int), compare);

  for (i = 0; i < N && x > 0; i++) {
    x -= a[i];
  }

  if (x != 0) {
    i--;
  }

  printf("%ld\n", i);

  return 0;
}
