#include <stdio.h>

int main() {
  int N, A, B;
  scanf("%d", &N);
  if (N == 1) {
    printf("Hello World\n");
  } else {
    scanf("%d", &A);
    scanf("%d", &B);
    printf("%d\n", A + B);
  }
  return 0;
}
