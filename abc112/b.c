#include <stdio.h>

int main() {
  int N, T, c, t;

  int i, min = 1001;

  scanf("%d %d", &N, &T);
  for (i = 0; i < N; i++) {
    scanf("%d %d", &c, &t);
    if (t <= T && c < min) {
      min = c;
    }
  }
  if (min <= 1000) {
    printf("%d\n", min);
  } else {
    printf("TLE\n");
  }
  return 0;
}
