#include <stdio.h>

int main() {
  int K;
  scanf("%d", &K);
  printf("%d\n", (K / 2) * (K / 2 + K % 2));
  return 0;
}
