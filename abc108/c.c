// WA
#include <stdio.h>

int main() {
  long int N, K, count = 0, a, b, c, max_b, max, mx, min_b, min;
  scanf("%ld %ld", &N, &K);
  mx = 2 * N / K;
  max = mx * K;
  for (a = N; a > 0; a--) {
    max_b = max - a;
    for (; max_b > N; max_b -= K)
      ;
    for (min = a / K; min * K <= a && min <= mx; min++)
      ;
    min_b = min * K - a;
    if (min_b < 1) {
      continue;
    }
    for (b = max_b; b >= min_b; b -= K) {
      for (c = max_b; c >= min_b; c -= K) {
        if ((b + c) % K == 0) {
          // printf("%ld %ld %ld\n", a, b, c);
          count++;
        }
      }
    }
  }

  // printf("\n%ld\n", count);
  printf("%ld\n", count);
  return 0;
}
