#include <stdio.h>

int main() {
  int x1, y1, x2, y2, x3, y3, x4, y4, dx, dy;
  scanf("%d %d %d %d", &x1, &y1, &x2, &y2);

  if (x1 < x2) {
    if (y1 < y2) {
      dx = x2 - x1;
      dy = y2 - y1;
      x3 = x2 - dy;
      y3 = y2 + dx;
      x4 = x1 - dy;
      y4 = y1 + dx;
    } else {
      dx = x2 - x1;
      dy = y1 - y2;
      x3 = x2 + dy;
      y3 = y2 + dx;
      x4 = x1 + dy;
      y4 = y1 + dx;
    }
  } else {
    if (y1 < y2) {
      dx = x1 - x2;
      dy = y2 - y1;
      x3 = x2 - dy;
      y3 = y2 - dx;
      x4 = x1 - dy;
      y4 = y1 - dx;
    } else {
      dx = x1 - x2;
      dy = y1 - y2;
      x3 = x2 + dy;
      y3 = y2 - dx;
      x4 = x1 + dy;
      y4 = y1 - dx;
    }
  }
  printf("%d %d %d %d\n", x3, y3, x4, y4);
  return 0;
}
