#include <stdio.h>
#include <string.h>

int main() {
  char S[200001], T[200001];

  long int i, j, k, len, l, tmpl, can_change;

  char c, d, tmpc;

  scanf("%s", S);
  scanf("%s", T);

  l = len = strlen(S);
  k = 0;
  for (i = 0; i < l; i++) {
    c = S[i];
    if (c >= 'a') {
      k++;
      tmpl = i;
      for (j = i; j < l; j++) {
        if ((tmpc = S[j]) == c) {
          S[j] = k;
          tmpl = j + 1;
        } else if (tmpc >= 'a') {
          tmpl = j + 1;
        }
      }
      l = tmpl;
    }
  }
  can_change = k >= 26;

  l = len;
  k = 0;
  for (i = 0; i < l; i++) {
    c = T[i];
    if (c >= 'a') {
      k++;
      tmpl = i;
      for (j = i; j < l; j++) {
        if ((tmpc = T[j]) == c) {
          T[j] = k;
          tmpl = j + 1;
        } else if (tmpc >= 'a') {
          tmpl = j + 1;
        }
      }
      l = tmpl;
    }
  }

  for (i = 0; i < len; i++) {
    c = S[i];
    d = T[i];
    if (c < d) {
      printf("No\n");
      return 0;
    } else if (c != d) {
      if (!can_change) {
        printf("No\n");
        return 0;
      }
      for (j = i; j < len; j++) {
        if ((tmpc = S[j]) == c) {
          S[j] = d;
        }
      }
    }
  }

  printf("Yes\n");
  return 0;
}
