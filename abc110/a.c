#include <stdio.h>

int main() {
  int A, B, C;
  scanf("%d %d %d", &A, &B, &C);
  if (A >= B && A >= C) {
    printf("%d\n", (A * 10) + B + C);
  } else if (B >= A && B >= C) {
    printf("%d\n", A + (B * 10) + C);
  } else {
    printf("%d\n", A + B + (C * 10));
  }
  return 0;
}
