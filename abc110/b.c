#include <stdio.h>

int main() {
  int N, M, X, Y;
  int tmp;

  int max;
  int min;

  int i;

  scanf("%d %d %d %d", &N, &M, &X, &Y);

  max = X;
  min = Y;

  for (i = 0; i < N; i++) {
    scanf("%d", &tmp);
    if (tmp > max) {
      max = tmp;
    }
  }

  for (i = 0; i < M; i++) {
    scanf("%d", &tmp);
    if (tmp < min) {
      min = tmp;
    }
  }

  if (max < min) {
    printf("No War\n");
  } else {
    printf("War\n");
  }
  return 0;
}
